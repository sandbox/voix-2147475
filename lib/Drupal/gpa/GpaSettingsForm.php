<?php

namespace Drupal\gpa;

use Drupal\Core\Form\ConfigFormBase;

/**
* Form constructor for the administrator configuration of the Google+ Authentication module.
*/

class GpaSettingsForm extends ConfigFormBase
{
  
  public function getFormId()
  {
    return 'gpa_configure';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state)
  {
    $config = $this->configFactory->get('gpa.settings');
    
    $form = array();
    
    $form['gpa'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
    );
    
    $form['gpa']['google_client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Google client ID'),
      '#description' => t('Create this with !link', array(
        '!link' => l(t('Google Console'), 'https://code.google.com/apis/console', array(
          'attributes' => array(
            'target' => '_blank',
          )
        ))
      )),
      '#default_value' => $config->get('google_client_id'),
    );
    
    
    $form['gpa']['google_client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Google client secret'),
      '#default_value' => $config->get('google_client_secret'),
    );
    
    $form['gpa']['google_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Google API key'),
      '#default_value' => $config->get('google_api_key'),
    );
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $this->configFactory->get('gpa.settings')->set('google_client_id', $form_state['values']['google_client_id'])->set('google_client_secret', $form_state['values']['google_client_secret'])->set('google_api_key', $form_state['values']['google_api_key'])->save();
    
    parent::submitForm($form, $form_state);
  }
  
}
