<?php
namespace Drupal\gpa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\user\RegisterFormController;
use Drupal\user;
use Google_Client;
use Google_PlusService;

require_once '/sites/all/libraries/google-api-php-client/Google_Client.php';
require_once '/sites/all/libraries/google-api-php-client/contrib/Google_PlusService.php';

class GpaController extends ControllerBase
{
  
  public function setupGoogleClient()
  {
    //Retrieve client ID, client secret, API key
    $config = $this->config('gpa.settings');
    $client_id = $config->get('google_client_id');
    $client_secret = $config->get('google_client_secret');
    $api_key = $config->get('google_api_key');
    
    //Setup request
    $client = new Google_Client();
    $client->setApplicationName('Google+ server-side flow');
    $client->setClientId($client_id);
    $client->setClientSecret($client_secret);
    $client->setRedirectUri($GLOBALS['base_url'] . "/gpa/redirect");
    $client->setDeveloperKey($api_key);
    
    return $client;
  }
  
  //Checks if a user is already registered with this Google+ account
  public function existsUser($gid)
  {
    $sql = "SELECT gpa_gid FROM users WHERE gpa_gid = :gid";
    $result = db_query($sql, array(
      ':gid' => $gid,
    ));
    $strings = $result->fetchAll();
    
    return (bool) count($strings);
  }
  
  //Saves a user
  public function saveUser($details)
  {
    
    $fields = array(
      'name' => $details['username'] . $details['gid'],
      'status' => 1,
      'created' => time(),
    );
    
    //Create user
    $account = entity_create('user', $fields);
    $account->save();
    
    //Rename user
    $account->set('name', $details['username'] . $account->id());
    $account->save();
    
    //Update user Google ID with db_update, as the User entity does not allow custom fields
    $update_gid = db_update('users')->fields(array('gpa_gid' => $details['gid']))->condition('uid', $account->id(), '=')->execute();
    
    return $account;
  }
  
  //Select values from a user based on its Google ID
  public function selectUserByGid($field, $gid)
  {
    $sql = "SELECT * FROM users WHERE gpa_gid = :gid LIMIT 1";
    $result = db_query($sql, array(':gid' => $gid))->fetchAssoc();
    return $result[$field];
  }
  
  public function gpaSignin()
  {
    
    //Redirect if user is logged in
    if (user_is_logged_in()) {
      return ControllerBase::redirect('user.login', array());
    }
    
    $client = $this->setupGoogleClient();
    
    $_SESSION['csrf'] = mt_rand();
    
    //Setup CSRF token
    $csrf_token = \Drupal::csrfToken()->get($_SESSION['csrf']);
    
    //Setup client
    $client->setState($csrf_token);
    $plus = new Google_PlusService($client);
    $auth_url = $client->createAuthUrl();
    
    $output = array();
    $output['gpa'] = array(
      '#markup' => "<a href='{$auth_url}' id='signInButton'><img src='modules/gpa/sign-in-with-google-button.png'></a>",
    );
    return $output;
  }
  
  public function gpaRedirect()
  {
    
    global $user;
    
    //Setup client
    $client = $this->setupGoogleClient();
    $plus   = new Google_PlusService($client);
    
    //If state is not set, redirect to userpage
    if (!isset($_GET['state'])) {
      return ControllerBase::redirect('user.login', array());
    }
    
    //If no authorization code passed (i.e., did not authenticate), redirect to user login
    if (!isset($_GET['code'])) {
      return ControllerBase::redirect('user.login', array());
    }
    
    //If state is invalid, exit
    $state_is_valid = \Drupal::csrfToken()->validate($_GET['state'], $_SESSION['csrf']);
    if (!$state_is_valid) {
      return ControllerBase::redirect('user.login', array());
    }
    
    //Authenticate
    $client->authenticate();
    $_SESSION['token'] = $client->getAccessToken();
    
    //Set access token for retrieval
    if (isset($_SESSION['token'])) {
      $client->setAccessToken($_SESSION['token']);
    }
    
    //Retrieve data from Google
    if ($client->getAccessToken()) {
      $me = $plus->people->get('me');
      
      //Update
      $_SESSION['token'] = $client->getAccessToken();
      
      //Register if not already registered
      if (!$this->existsUser($me['id'])) {
        $user_details = array(
          'username' => $me['name']['givenName'] . $me['name']['familyName'],
          'gid' => $me['id'],
        );
        $local_user = $this->saveUser($user_details);
      } else {
        $local_user = user_load($this->selectUserByGid('uid', $me['id']));
      }
      //Login user as local user
      user_login_finalize($local_user);
      
      //Redirect to user page
      return ControllerBase::redirect('<front>', array());
    }
    
    //Otherwise, output error message...
    $output = array();
    $output['gpa'] = array(
      '#markup' => 'An error occurred. Please try signing in again.',
    );
    return $output;
  }
  
}
?>