-Installation-

This is the Google+ Authentication module for Drupal 8.

- Extract this module to the modules folder. 
- Download the Google APIs Client Library for PHP (this module tested with 0.6.6) at http://code.google.com/p/google-api-php-client and extract to {$drupalRoot}/sites/all/libraries. You may need to create this directory.
- Enable this module.
- Set up an account at Google's Developer Console page, https://code.google.com/apis/console.
- Create a new project. Retrieve your API key, client ID, and client secret.
- Edit the redirect URI to http://example.com/gpa/redirect, replacing {example.com} with your site domain.
- Configure the module by pasting these into the form at /admin/config/services/gpa.
- Log out. You will be able to sign in with Google+.